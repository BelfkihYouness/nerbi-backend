const Mongoose = require('mongoose');
const BCrypt = require('bcrypt');

const ObjectID = Mongoose.SchemaTypes.ObjectId;

const ShopSchema = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    location: { // GeoJSON
        type: { type: String },
        coordinates: [Number],
    }
});

ShopSchema.index({ "location": "2dsphere" });

ShopSchema.statics.findGeoNearbyShops = ( longitude, latitude, options = {} ) => {
    return  new Promise(( resolve, reject ) => {
        Shop.aggregate([
            { '$geoNear': Object.assign({
                'near': {
                    'type': 'Point',
                    'coordinates': [longitude, latitude]
                },
                'distanceField': 'distance',
                'spherical': true,
                'key': 'location'
            }, options) },
            { '$limit': 30 },
            { '$project': {
                'name': true,
                'distance': true
            } },
            // Note: no need for $sort: $geoNear results in a sorted set of documents.
            // { '$sort': { 'distance': 1 } }.
        ]).then( resolve, reject )
    });
};

ShopSchema.statics.findShop = ( id ) => new Promise(( resolve, reject ) => {
    Shop.findById(id).then(resolve, reject);
});

ShopSchema.statics.saveToDB = function (shop) {
    return new Promise(( resolve, reject ) => {
        shop.save(( error, document ) => {
            if ( error )
                reject( error );
            else
                resolve( document );
        });
    });
};

const UserSchema = new Mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    saved: [{
        type: ObjectID,
        ref: ShopSchema
    }],
    hidden: [{
        shop: {
            type: ObjectID,
            unique: true,
            ref: ShopSchema
        },
        timeout: Date
    }]
});

UserSchema.statics.authenticate = ( email, password ) => new Promise(( resolve, reject ) => {
    User.findOne({ email }, {
        email: true,
        password: true,
    }).then((user) => {
        BCrypt.compare( password, user.password, ( error, same ) => {
            if ( error )
                return reject( error );
            if ( same )
                return resolve( user );
            reject(new Error('Incorrect password.'));
        });
    }, reject);
});

UserSchema.statics.getShopPrefs = ( id ) => new Promise(( resolve, reject ) => {
    User.findById(id, {
        hidden: true,
        saved: true,
    }).then(resolve, reject);
});

UserSchema.statics.saveToDB = function (user) {
    return new Promise(( resolve, reject ) => {
        user.save(( error, document ) => {
            if ( error )
                reject( error );
            else
                resolve( document );
        });
    });
};

const User = exports.User = Mongoose.model('User', UserSchema);
const Shop = exports.Shop = Mongoose.model('Shop', ShopSchema);