const Mongoose = require("mongoose");
const Express = require("express");

const BodyParser = require("body-parser");
const HistoryAPI = require('connect-history-api-fallback');

Mongoose.Promise = Promise;

const app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: false }));

require("./routes/users")(app);
require("./routes/shops")(app);

app.use('/api', function ( error, request, response, next ) {
    console.error(error);
    res.status(error.status || 500).send({ 'success': false, 'error': error.message });
});

app.use('/', HistoryAPI());
app.use('/', Express.static('../client/dist'));

Mongoose.connect("mongodb://localhost/nerbi", {
    useMongoClient: true
}, function(error) {
    if (error) {
        console.log("Could not establish a connection to MongoDB");
    } else {
        app.listen(3000, function() {
            console.log("Connected on port 3000...");
        });
    }
});