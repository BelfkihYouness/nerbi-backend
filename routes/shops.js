const Express = require('express');

const { User, Shop } = require('../models');
const { JWT, Async } = require('../utils');

/**
 * @param {Express.Application} app
 */
module.exports = (app) => {

    const shops = Express.Router({
        caseSensitive: true,
        strict: true,
    });

    app.use( '/api/shops', shops );
    
    const HIDE_TIMEOUT = 2 * 60 * 60 * 1000; // 2 HOURS

    function createHidingTimeout() {
        return new Date(Date.now() + HIDE_TIMEOUT);
    }

    shops.post('/', async ( request, response ) => {

        try {

            const { token, longitude, latitude } = request.body;

            const { id } = JWT.verify( token );

            let [ shops, prefs ] = await Async.run([
                Shop.findGeoNearbyShops(
                    parseFloat(longitude),
                    parseFloat(latitude)
                ),
                User.getShopPrefs( id )
            ], 3000, 'Database query timed out.' );

            const now = new Date();

            let hidden = prefs.hidden;
            const saved = prefs.saved;

            hidden = hidden.filter((shop) => {
                return shop.timeout >= now;
            });

            prefs.hidden = hidden;

            debugger;

            await User.saveToDB(prefs);

            shops = shops.filter((shop) => {

                const shopID = shop._id;
                return !(
                    saved.some((shop) => shop.equals(shopID)) ||
                    hidden.some((hidden) => hidden.shop.equals(shopID))
                );
            });

            response.send({ 'success': true, shops });

        } catch ( error ) {
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });
        }

    });

    shops.post('/saved', async ( request, response ) => {
        
        const { token, longitude, latitude } = request.body;

        try {

            const { id } = JWT.verify( token );

            let [ shops, user ] = await Async.run([
                Shop.findGeoNearbyShops(
                    parseFloat(longitude),
                    parseFloat(latitude)
                ),
                User.getShopPrefs( id )
            ], 3000, 'Database query timed out.' );

            const saved = user.saved;

            shops = shops.filter((shop) => {
                const shopID = shop._id;
                return saved.some((shop) => shop.equals(shopID));
            });

            response.send({ 'success': true, shops });

        } catch ( error ) {
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });
        }

    });

    shops.post('/save', async ( request, response ) => {

        try {

            const { token, id: shopID } = request.body;

            const { id } = JWT.verify( token );

            const [ prefs, shop ] = await Async.run([
                User.getShopPrefs( id ),
                Shop.findShop( shopID )
            ], 3000, 'Database query timed out.' );

            const saved = prefs.saved;

            {
                const shopID = shop._id;
                const index = saved.findIndex((id) => {
                    return id.equals(shopID);
                })
    
                if ( index !== -1 ) {
                    throw new Error('Shop already saved');
                } else {
                    saved.push(shopID);
                }
            }

            prefs.saved = saved;
            await User.saveToDB(prefs);

            response.send({ 'success': true });

        } catch ( error ) {
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });
        }

    });

    shops.post('/unsave', async ( request, response ) => {

        try {

            const { token, id: shopID } = request.body;

            const { id } = JWT.verify( token );

            const [ prefs, shop ] = await Async.run([
                User.getShopPrefs( id ),
                Shop.findShop( shopID )
            ], 3000, 'Database query timed out.' );

            const saved = prefs.saved;

            {
                const shopID = shop._id;
                const index = saved.findIndex((id) => {
                    return id.equals(shopID);
                })
    
                if ( index !== -1 ) {
                    saved.splice(index, 1);
                } else {
                    throw new Error('Shop not yet saved');
                }
            }

            prefs.saved = saved;
            await User.saveToDB(prefs);

            response.send({ 'success': true });

        } catch ( error ) {
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });
        }

    });

    shops.post('/hide', async ( request, response ) => {

        try {

            const { token, id: shopID } = request.body;

            const { id } = JWT.verify( token );

            const [ prefs, shop ] = await Async.run([
                User.getShopPrefs( id ),
                Shop.findShop( shopID )
            ], 3000, 'Database query timed out.' );

            const now = new Date();

            let hidden = prefs.hidden;

            {

                const shopID = shop._id;
                const index = hidden.findIndex((shop) => {
                    return shop.shop.equals(shopID);
                })
    
                if ( index !== -1 ) {
                    hidden[index].timeout = createHidingTimeout();
                } else {
                    hidden.push({
                        shop: shopID,
                        timeout: createHidingTimeout(),
                    });
                }

            }

            hidden = hidden.filter((shop) => {
                return shop.timeout >= now;
            });

            prefs.hidden = hidden;
            await User.saveToDB(prefs);

            response.send({ 'success': true });

        } catch ( error ) {
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });
        }

    });

    return shops;

};

