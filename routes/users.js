const Express = require('express');
const BCrypt = require('bcrypt');

const { User } = require('../models');
const { JWT } = require('../utils');

/**
 * @param {Express.Application} app
 */
module.exports = (app) => {

    const users = Express.Router({
        caseSensitive: true,
        strict: true
    });

    app.use( '/api', users );

    users.post( '/register', async ( request, response ) => {

        try {

            let { password, email } = request.body;

            password = await BCrypt.hash(password, 10);

            const user = new User({ email, password });

            await User.saveToDB( user );

            response.send({ 'success': true, token: JWT.sign( user._id, user.email ) });

        } catch ( error ) {
            
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });

        }

    });

    users.post( '/token', ( request, response ) => {

        const { email, password } = request.body;

        User.authenticate( email, password ).then(( user ) => {
            response.send({ 'success': true, token: JWT.sign( user._id, user.email ) });
        }, ( error ) => {
            console.error( error );
            response.status(401).send({ 'success': false, 'message': `${error}` });
        });

    });

    return users;

};