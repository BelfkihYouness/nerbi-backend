# NERBI

An app to "locate" & save nearby shops.

Client repo: https://gitlab.com/BelfkihYouness/nerbi

## Install

```
# inside nerbi/server/
$ yarn install
# inside nerbi/client/
$ yarn install
```

## Starting the server

```
# inside nerbi/server/
$ yarn demon
```

## Adding more Shops

I added a script to add ten shops to the database everytime you call it.

```
# inside nerbi/server/
$ yarn shops
```

## Notes

#### Session vs JSON Web Token

Had a problem with the session data not being passed through,
from the backend server to frontend dev server.

So I switched to using a JWT backend API.

#### Online documentations

Midway through my ISP (IAM), dropped my Internet service, by error.
I settled the issue with them, and service will be back soon, (supposedly 9/1/2019).

As result I haven't been able to do more research on the libraries I chose.

For `Bulma`, guess work was somewhat fruitfull.

But for `Axios`, I failed to get error state from it.
I fiddled with it a bit then decided to move on.

As such the *Sign in* and *Sign up* forms fail state is were not implemented.

Maybe I should of used `fetch` API, but then again, I haven't learned to use it
in REST or CRUD situations and would have to lookup online.

`XHttpRequest` API, is overkill.