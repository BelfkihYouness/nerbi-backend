const Mongoose = require("mongoose");

Mongoose.Promise = Promise;

const { Shop } = require('../models');

Mongoose.connect("mongodb://localhost/nerbi", {
    useMongoClient: true
}, function(error) {
    if (error) {
        console.log("Could not establish a connection to MongoDB");
    } else {
        Promise.all(Array.from({ length: 10 }, (_, i) => create( start + i ))).then(() => {
            console.log('Created 10 shops.');
            process.exit( 0 );
        });
    }
});

const start = Math.random() * 1000 | 0;

function create( num ) {
    return Shop.saveToDB(new Shop({
        'name': `Shop Name ${num}`,
        'location': {
            'type': 'Point',
            'coordinates': [
                Math.random(),
                Math.random()
            ]
        }
    }));
}