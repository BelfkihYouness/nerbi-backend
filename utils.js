// @ts-check

const JWT = require('jsonwebtoken');

const SECRET_KEY = process.env.SECRET_KEY || 'united-remote';

exports.JWT = {

    /**
     * @param { string } id
     * @param { string } email
     */
    sign(id, email, options = {}) {
        return JWT.sign({ id, email }, SECRET_KEY, options);
    },

    /**
     * @param { string } token
     * @returns {{ id: string, email: string }}
     */
    verify(token, options = {}) {
        // @ts-ignore
        return JWT.verify(token, SECRET_KEY, options);
    },

};

const Async = exports.Async = {

    /**
     * Rejects a promise after a an amount of ms.
     * @param {number} timeout 
     * @param {string} message
     */
    timeout( timeout, message ) {
        return new Promise(( _, reject ) => {
            setTimeout(reject.bind(null, new Error(message)), timeout);
        });
    },

    /**
     * Rejects a promise after a an amount of ms.
     * @param {Promise<any>[]} tasks
     * @param {number} timeout
     * @param {string} message
     * @returns {Promise<any[]>}
     */
    async run( tasks, timeout, message ) {
        return Promise.race([
            Promise.all(tasks),
            Async.timeout( timeout, message )
        ]);
    }

};